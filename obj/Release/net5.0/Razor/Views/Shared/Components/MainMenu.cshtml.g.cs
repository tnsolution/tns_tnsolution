#pragma checksum "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b94dd79eff765306e728374468e2755a050365da"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_Components_MainMenu), @"mvc.1.0.view", @"/Views/Shared/Components/MainMenu.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\_Project\_webbasic\WebSite\Views\_ViewImports.cshtml"
using WebSite;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\_Project\_webbasic\WebSite\Views\_ViewImports.cshtml"
using WebSite.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b94dd79eff765306e728374468e2755a050365da", @"/Views/Shared/Components/MainMenu.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b9751a11891ba42cf18e84a4b82e03210f652818", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_Components_MainMenu : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<Menu_WebSite_Model>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Home", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Contact", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
  
    var ListParent = Model.Where(s => s.Parent == 0).ToList();

#line default
#line hidden
#nullable disable
            WriteLiteral("<div class=\"main-header\">\r\n    <div class=\"container\">\r\n        <div class=\"row align-items-center\">\r\n            <div class=\"col-lg-2 col-xl-2 col-md-6 col-6\">\r\n                <div class=\"header-logo d-flex align-items-center\">\r\n                    <a");
            BeginWriteAttribute("href", " href=\"", 357, "\"", 392, 1);
#nullable restore
#line 10 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
WriteAttributeValue("", 364, Url.Action("Index", "Home"), 364, 28, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                        <img class=\"nonsticky-logo img-full\"");
            BeginWriteAttribute("src", " src=\"", 456, "\"", 488, 1);
#nullable restore
#line 11 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
WriteAttributeValue("", 462, Url.Content(ViewBag.Logo), 462, 26, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" alt=\"Header Logo\" width=\"92\"\r\n                             style=\"border-radius: 50px; padding: 1px; background: white; width:90px\">\r\n                        <img class=\"sticky-logo img-full\"");
            BeginWriteAttribute("src", " src=\"", 681, "\"", 713, 1);
#nullable restore
#line 13 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
WriteAttributeValue("", 687, Url.Content(ViewBag.Logo), 687, 26, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" alt=""Header Logo"" width=""92""
                             style="" border-radius: 50px; padding: 1px; background: white; width: 90px"">
                    </a>
                </div>
            </div>
            <div class=""col-lg-10 col-md-6 col-6 d-flex justify-content-end"">
                <nav class=""main-nav d-none d-lg-flex"">
                    <ul class=""nav"">
                        <li>
                            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b94dd79eff765306e728374468e2755a050365da6282", async() => {
                WriteLiteral("\r\n                                <span class=\"menu-text\">Trang chủ</span>\r\n                            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                        </li>\r\n");
#nullable restore
#line 26 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
                          
                            foreach (var parent in ListParent)
                            {
                                #region [----]
                                string parentlink = "#";
                                string parentName = parent.MenuName;
                                string parentParamater = parent.Paramater;
                                string parentAction = parent.Action;
                                int parentType = parent.TypeKey;
                                #endregion

                                var ListChild = Model.Where(s => s.Parent == parent.MenuKey).ToList();
                                if (ListChild.Count > 0)
                                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                    <li>\r\n                                        <a href=\"#\">\r\n                                            <span class=\"menu-text\">\r\n                                                ");
#nullable restore
#line 43 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
                                           Write(parentName);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
                                            </span>
                                            <i class=""fa fa-angle-down""></i>
                                        </a>
                                        <ul class=""dropdown-submenu dropdown-hover"">
");
#nullable restore
#line 48 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
                                              
                                                foreach (var child in ListChild)
                                                {
                                                    #region [----]
                                                    string childName = child.MenuName;
                                                    string childAction = child.Action;
                                                    string childParamater = child.Paramater;
                                                    int childType = child.TypeKey;
                                                    string childLink = "#";
                                                    #endregion

                                                    if (childType == 1)
                                                    {
                                                        childLink = "/bai-viet/" + childParamater;
                                                    }
                                                    else
                                                    {
                                                        childLink = "/tin-tuc/" + childParamater;
                                                    }


#line default
#line hidden
#nullable disable
            WriteLiteral("                                                    <li>\r\n                                                        <a");
            BeginWriteAttribute("href", " href=\"", 3996, "\"", 4013, 1);
#nullable restore
#line 69 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
WriteAttributeValue("", 4003, childLink, 4003, 10, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 69 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
                                                                        Write(childName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                                                    </li>\r\n");
#nullable restore
#line 71 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
                                                }
                                            

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        </ul>\r\n                                    </li>\r\n");
#nullable restore
#line 75 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
                                }
                                else
                                {
                                    if (parentType == 1)
                                    {
                                        parentlink = "/bai-viet/" + parentParamater;
                                    }
                                    else
                                    {
                                        parentlink = "/tin-tuc/" + parentParamater;
                                    }


#line default
#line hidden
#nullable disable
            WriteLiteral("                                    <li>\r\n                                        <a");
            BeginWriteAttribute("href", " href=\"", 4899, "\"", 4917, 1);
#nullable restore
#line 88 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
WriteAttributeValue("", 4906, parentlink, 4906, 11, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                            <span class=\"menu-text\">\r\n                                                ");
#nullable restore
#line 90 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
                                           Write(parentName);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                            </span>\r\n                                        </a>\r\n                                    </li>\r\n");
#nullable restore
#line 94 "C:\_Project\_webbasic\WebSite\Views\Shared\Components\MainMenu.cshtml"
                                }
                            }
                        

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <li>\r\n                            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "b94dd79eff765306e728374468e2755a050365da14135", async() => {
                WriteLiteral("\r\n                                <span class=\"menu-text\">Liên hệ</span>\r\n                            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                        </li>
                        <li>
                            <a href=""http://shop.tnsolution.vn/"" target=""_blank"">
                                <span class=""menu-text"">Shop điện tử</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class=""header-right-area main-nav"">
                    <ul class=""nav"">
                        <li class=""mobile-menu-btn d-block d-lg-none"">
                            <a class=""off-canvas-btn"" href=""#"">
                                <i class=""fa fa-bars""></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<Menu_WebSite_Model>> Html { get; private set; }
    }
}
#pragma warning restore 1591
