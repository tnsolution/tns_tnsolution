#pragma checksum "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a783082481e11d04d95f108b84e974d9d58efb38"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Admin__Layout), @"mvc.1.0.view", @"/Views/Admin/_Layout.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\_Project\_webbasic\WebSite\Views\_ViewImports.cshtml"
using WebSite;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\_Project\_webbasic\WebSite\Views\_ViewImports.cshtml"
using WebSite.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a783082481e11d04d95f108b84e974d9d58efb38", @"/Views/Admin/_Layout.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b9751a11891ba42cf18e84a4b82e03210f652818", @"/Views/_ViewImports.cshtml")]
    public class Views_Admin__Layout : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/themes/admin/img/avatar.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("Joseph Doe"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("rounded-circle"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("data-lock-picture", new global::Microsoft.AspNetCore.Html.HtmlString("img/!logged-user.jpg"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("object-fit:cover"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("height", new global::Microsoft.AspNetCore.Html.HtmlString("35"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("role", new global::Microsoft.AspNetCore.Html.HtmlString("menuitem"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_7 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Auth", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_8 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_9 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("loading-overlay-showing"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
  
    var logo = HttpContextAccessor.HttpContext.Request.Cookies["logo"];
    if (logo == null || logo == string.Empty)
    {
        logo = "~/themes/admin/img/logodefault.png";
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("<!DOCTYPE html>\r\n\r\n<html class=\"fixed sidebar-light\">\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a783082481e11d04d95f108b84e974d9d58efb387277", async() => {
                WriteLiteral(@"
    <meta charset=""utf-8"" />
    <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
    <title>Quản trị nội dung</title>
    <!-- Web Fonts  -->
    <link href=""https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Shadows+Into+Light"" rel=""stylesheet"" type=""text/css"">

    <!-- Vendor CSS -->
    <link rel=""stylesheet""");
                BeginWriteAttribute("href", " href=\"", 699, "\"", 771, 1);
#nullable restore
#line 20 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 706, Url.Content("~/themes/admin/vendor/bootstrap/css/bootstrap.css"), 706, 65, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 803, "\"", 874, 1);
#nullable restore
#line 21 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 810, Url.Content("~/themes/admin/vendor/animate/animate.compat.css"), 810, 64, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 908, "\"", 981, 1);
#nullable restore
#line 23 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 915, Url.Content("~/themes/admin/vendor/font-awesome/css/all.min.css"), 915, 66, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1013, "\"", 1087, 1);
#nullable restore
#line 24 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 1020, Url.Content("~/themes/admin/vendor/boxicons/css/boxicons.min.css"), 1020, 67, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1119, "\"", 1197, 1);
#nullable restore
#line 25 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 1126, Url.Content("~/themes/admin/vendor/magnific-popup/magnific-popup.css"), 1126, 71, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1229, "\"", 1324, 1);
#nullable restore
#line 26 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 1236, Url.Content("~/themes/admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css"), 1236, 88, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n\r\n    <!-- Specific Page Vendor CSS -->\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1397, "\"", 1465, 1);
#nullable restore
#line 29 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 1404, Url.Content("~/themes/admin/vendor/jquery-ui/jquery-ui.css"), 1404, 61, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1497, "\"", 1571, 1);
#nullable restore
#line 30 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 1504, Url.Content("~/themes/admin/vendor/jquery-ui/jquery-ui.theme.css"), 1504, 67, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1603, "\"", 1685, 1);
#nullable restore
#line 31 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 1610, Url.Content("~/themes/admin/vendor/jquery-confirm/css/jquery-confirm.css"), 1610, 75, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1717, "\"", 1785, 1);
#nullable restore
#line 32 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 1724, Url.Content("~/themes/admin/vendor/select2/css/select2.css"), 1724, 61, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1817, "\"", 1888, 1);
#nullable restore
#line 33 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 1824, Url.Content("~/themes/admin/vendor/pnotify/pnotify.custom.css"), 1824, 64, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <!-- themes CSS -->\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 1945, "\"", 1996, 1);
#nullable restore
#line 35 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 1952, Url.Content("~/themes/admin/css/theme.css"), 1952, 44, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <!-- Skin CSS -->\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 2051, "\"", 2110, 1);
#nullable restore
#line 37 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 2058, Url.Content("~/themes/admin/css/skins/default.css"), 2058, 52, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    <link rel=\"stylesheet\"");
                BeginWriteAttribute("href", " href=\"", 2142, "\"", 2194, 1);
#nullable restore
#line 38 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 2149, Url.Content("~/themes/admin/css/custom.css"), 2149, 45, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" />\r\n    ");
#nullable restore
#line 39 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
Write(await RenderSectionAsync("Styles", false));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    <!-- Head Libs -->\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 2283, "\"", 2349, 1);
#nullable restore
#line 41 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 2289, Url.Content("~/themes/admin/vendor/modernizr/modernizr.js"), 2289, 60, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a783082481e11d04d95f108b84e974d9d58efb3815503", async() => {
                WriteLiteral(@"
    <div class=""loading-overlay"">
        <div class=""bounce-loader"">
            <div class=""bounce1""></div>
            <div class=""bounce2""></div>
            <div class=""bounce3""></div>
        </div>
    </div>
    <section class=""body"">
        <header class=""header"">
            <div class=""logo-container"">
                <a");
                BeginWriteAttribute("href", " href=\"", 2776, "\"", 2810, 1);
#nullable restore
#line 54 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 2783, Url.Action("Index","Home"), 2783, 27, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" class=\"logo\">\r\n                    <img");
                BeginWriteAttribute("src", " src=\"", 2851, "\"", 2875, 1);
#nullable restore
#line 55 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 2857, Url.Content(logo), 2857, 18, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(@" height=""35"" alt=""logo"">
                </a>
                <div class=""d-md-none toggle-sidebar-left""
                     data-toggle-class=""sidebar-left-opened""
                     data-target=""html""
                     data-fire-event=""sidebar-left-opened"">
                    <i class=""fas fa-bars"" aria-label=""Toggle sidebar""></i>
                </div>
            </div>

            <!-- start: search & user box -->
            <div class=""header-right"">
                <div id=""userbox"" class=""userbox"">
                    <a href=""#"" data-toggle=""dropdown"">
                        <figure class=""profile-picture"">
                            ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "a783082481e11d04d95f108b84e974d9d58efb3817594", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral(@"
                        </figure>
                        <div class=""profile-info"" data-lock-name=""John Doe"" data-lock-email="".."">
                            <span class=""name"">Quản trị viên</span>
                        </div>
                        <i class=""fa custom-caret""></i>
                    </a>
                    <div class=""dropdown-menu"">
                        <ul class=""list-unstyled mb-2"">
                            <li>
                                ");
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "a783082481e11d04d95f108b84e974d9d58efb3819620", async() => {
                    WriteLiteral("\r\n                                    <i class=\"fas fa-power-off\"></i>\r\n                                    Đăng xuất\r\n                                ");
                }
                );
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
                __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_7.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_7);
                __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_8.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_8);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                WriteLiteral(@"
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end: search & user box -->
        </header>
        <div class=""inner-wrapper"" style=""background-image: url('')"">
            <aside id=""sidebar-left"" class=""sidebar-left""");
                BeginWriteAttribute("style", " style=\"", 4751, "\"", 4759, 0);
                EndWriteAttribute();
                WriteLiteral(@">
                <div class=""sidebar-header"">
                    <div class=""sidebar-title"">

                    </div>
                    <div class=""sidebar-toggle d-none d-md-block"" data-toggle-class=""sidebar-left-collapsed"" data-target=""html"" data-fire-event=""sidebar-left-toggle"">
                        <i class=""fas fa-bars"" aria-label=""Toggle sidebar""></i>
                    </div>
                </div>
                <div class=""nano"">
                    <div class=""nano-content"">
                        <nav id=""menu"" class=""nav-main"" role=""navigation"">
                            ");
#nullable restore
#line 104 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
                       Write(await Component.InvokeAsync("AdminMenu"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n                        </nav>\r\n                    </div>\r\n                </div>\r\n            </aside>\r\n            <section role=\"main\" class=\"content-body content-body-modern mt-0\">\r\n                ");
#nullable restore
#line 110 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
           Write(RenderBody());

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n            </section>\r\n        </div>\r\n    </section>\r\n\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 5707, "\"", 5771, 1);
#nullable restore
#line 115 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 5713, Url.Content("~/themes/admin/vendor/jquery/jquery.min.js"), 5713, 58, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 5795, "\"", 5885, 1);
#nullable restore
#line 116 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 5801, Url.Content("~/themes/admin/vendor/jquery-browser-mobile/jquery.browser.mobile.js"), 5801, 84, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 5909, "\"", 5977, 1);
#nullable restore
#line 117 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 5915, Url.Content("~/themes/admin/vendor/popper/umd/popper.min.js"), 5915, 62, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6001, "\"", 6070, 1);
#nullable restore
#line 118 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 6007, Url.Content("~/themes/admin/vendor/bootstrap/js/bootstrap.js"), 6007, 63, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6094, "\"", 6154, 1);
#nullable restore
#line 119 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 6100, Url.Content("~/themes/admin/vendor/common/common.js"), 6100, 54, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6178, "\"", 6250, 1);
#nullable restore
#line 120 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 6184, Url.Content("~/themes/admin/vendor/nanoscroller/nanoscroller.js"), 6184, 66, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6274, "\"", 6357, 1);
#nullable restore
#line 121 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 6280, Url.Content("~/themes/admin/vendor/magnific-popup/jquery.magnific-popup.js"), 6280, 77, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6381, "\"", 6441, 1);
#nullable restore
#line 122 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 6387, Url.Content("~/themes/admin/vendor/moment/moment.js"), 6387, 54, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <!-- themes Base, Components and Settings -->\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6516, "\"", 6585, 1);
#nullable restore
#line 124 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 6522, Url.Content("~/themes/admin/vendor/pnotify/pnotify.custom.js"), 6522, 63, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6609, "\"", 6674, 1);
#nullable restore
#line 125 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 6615, Url.Content("~/themes/admin/vendor/select2/js/select2.js"), 6615, 59, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6698, "\"", 6789, 1);
#nullable restore
#line 126 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 6704, Url.Content("~/themes/admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"), 6704, 85, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6813, "\"", 6883, 1);
#nullable restore
#line 127 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 6819, Url.Content("~/themes/admin/vendor/ios7-switch/ios7-switch.js"), 6819, 64, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6907, "\"", 6971, 1);
#nullable restore
#line 128 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 6913, Url.Content("~/themes/admin/vendor/autosize/autosize.js"), 6913, 58, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 6995, "\"", 7075, 1);
#nullable restore
#line 129 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 7001, Url.Content("~/themes/admin/vendor/jquery-validation/jquery.validate.js"), 7001, 74, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7099, "\"", 7186, 1);
#nullable restore
#line 130 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 7105, Url.Content("~/themes/admin/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js"), 7105, 81, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7210, "\"", 7289, 1);
#nullable restore
#line 131 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 7216, Url.Content("~/themes/admin/vendor/jquery-confirm/js/jquery-confirm.js"), 7216, 73, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7313, "\"", 7369, 1);
#nullable restore
#line 132 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 7319, Url.Content("~/themes/admin/js/jquery.number.js"), 7319, 50, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7393, "\"", 7441, 1);
#nullable restore
#line 133 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 7399, Url.Content("~/themes/admin/js/theme.js"), 7399, 42, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <!-- themes Initialization Files -->\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7507, "\"", 7555, 1);
#nullable restore
#line 135 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 7513, Url.Content("~/themes/admin/js/Utils.js"), 7513, 42, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7579, "\"", 7637, 1);
#nullable restore
#line 136 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 7585, Url.Content("~/themes/admin/js/session-timeout.js"), 7585, 52, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7661, "\"", 7710, 1);
#nullable restore
#line 137 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 7667, Url.Content("~/themes/admin/js/custom.js"), 7667, 43, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    ");
#nullable restore
#line 138 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
Write(await RenderSectionAsync("Scripts", required: false));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7793, "\"", 7846, 1);
#nullable restore
#line 139 "C:\_Project\_webbasic\WebSite\Views\Admin\_Layout.cshtml"
WriteAttributeValue("", 7799, Url.Content("~/themes/admin/js/theme.init.js"), 7799, 47, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            BeginWriteTagHelperAttribute();
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __tagHelperExecutionContext.AddHtmlAttribute("data-loading-overlay", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_9);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n</html>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public Microsoft.AspNetCore.Http.IHttpContextAccessor HttpContextAccessor { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
