﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSite.Component
{
    [ViewComponent]
    public class AdminMenuViewComponent: ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var zList = Article_Category_Data.ListAsMenu(TN_Helper.PartnerNumber, out _);
            return View("~/Views/Admin/Components/Menu.cshtml", zList);
        }
    }
}
