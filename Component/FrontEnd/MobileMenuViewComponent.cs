﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace WebSite.Component
{
    [ViewComponent]
    public class MobileMenuViewComponent : ViewComponent
    {
       
        public IViewComponentResult Invoke()
        {
            var zList = Menu_WebSite_Data.ListShow(TN_Helper.PartnerNumber, out _);
            zList = zList.Where(s => s.Publish == true).ToList();
            return View("~/Views/Shared/Components/MobileMenu.cshtml", zList);
        }
    }
}
