﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace WebSite.Component
{
    [ViewComponent]
    public class IntroViewComponent: ViewComponent
    {
        public IViewComponentResult Invoke(string ID)
        {
            var zInfo = new Config_Info(ID, TN_Helper.PartnerNumber);
            ViewBag.Title = zInfo.Config.Name;

            var zParent = zInfo.Config.ConfigKey;

            var zList = Config_Data.List(TN_Helper.PartnerNumber, zParent, out _);
            var zRecord = zList.SingleOrDefault(s => s.ConfigID == "COT1");

            var Blog = new Article_Model();
            if (zRecord != null)
            {
                string Key = zRecord.Value;
                Blog = new Article_Info(Key).Article;
            }

            ViewBag.Blog = Blog;

            zRecord = zList.SingleOrDefault(s => s.ConfigID == "COT2");
            Blog = new Article_Model();
            if (zRecord != null)
            {
                string Key = zRecord.Value;
                Blog = new Article_Info(Key).Article;
            }
            ViewBag.Youtube = Blog;

            return View("~/Views/Shared/Components/Intro.cshtml");
        }
    }
}
