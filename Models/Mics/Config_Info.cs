﻿using System;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Config_Info
    {

        public Config_Model Config = new Config_Model();
        private string _Message = "";
        public string Code
        {
            get
            {
                if (_Message.Length >= 3)
                    return _Message.Substring(0, 3);
                else return "";
            }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        #region [ Constructor Get Information ]
        public Config_Info()
        {
        }
        public Config_Info(int ConfigKey)
        {
            string zSQL = "SELECT * FROM SYS_Config WHERE ConfigKey = @ConfigKey AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ConfigKey", SqlDbType.Int).Value = ConfigKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["ConfigKey"] != DBNull.Value)
                        Config.ConfigKey = int.Parse(zReader["ConfigKey"].ToString());
                    Config.ConfigID = zReader["ConfigID"].ToString();
                    Config.Name = zReader["Name"].ToString();
                    Config.Value = zReader["Value"].ToString();
                    if (zReader["Parent"] != DBNull.Value)
                        Config.Parent = int.Parse(zReader["Parent"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        Config.Rank = int.Parse(zReader["Rank"].ToString());
                    Config.PartnerNumber = zReader["PartnerNumber"].ToString();

                    if (zReader["Slug"] != DBNull.Value)
                        Config.Slug = int.Parse(zReader["Slug"].ToString());
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public Config_Info(string ConfigID, string PartnerNumber)
        {
            string zSQL = "SELECT * FROM SYS_Config WHERE ConfigID = @ConfigID AND PartnerNumber = @PartnerNumber AND RecordStatus != 99 ";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ConfigID", SqlDbType.NVarChar).Value = ConfigID;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["ConfigKey"] != DBNull.Value)
                        Config.ConfigKey = int.Parse(zReader["ConfigKey"].ToString());
                    Config.ConfigID = zReader["ConfigID"].ToString();
                    Config.Name = zReader["Name"].ToString();
                    Config.Value = zReader["Value"].ToString();
                    if (zReader["Parent"] != DBNull.Value)
                        Config.Parent = int.Parse(zReader["Parent"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        Config.Rank = int.Parse(zReader["Rank"].ToString());
                    Config.PartnerNumber = zReader["PartnerNumber"].ToString();

                    if (zReader["Slug"] != DBNull.Value)
                        Config.Slug = int.Parse(zReader["Slug"].ToString());
                    _Message = "200 OK";
                }
                else
                {
                    _Message = "404 Not Found";
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion

        #region [ Constructor Update Information ]

        public string Create_ServerKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Config ("
         + " ConfigID , Name , Value , Parent , Rank , Slug, PartnerNumber ) "
         + " VALUES ( "
         + " @ConfigID , @Name , @Value , @Parent , @Rank , @Slug, @PartnerNumber ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ConfigID", SqlDbType.NVarChar).Value = Config.ConfigID;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Config.Name;
                zCommand.Parameters.Add("@Value", SqlDbType.NVarChar).Value = Config.Value;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Config.Parent;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Config.Slug;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Config.Rank;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Config.PartnerNumber;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_ClientKey()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_Config("
         + " ConfigKey , ConfigID , Name , Value , Parent , Rank , Slug, PartnerNumber ) "
         + " VALUES ( "
         + " @ConfigKey , @ConfigID , @Name , @Value , @Parent , @Rank , @Slug, @PartnerNumber ) ";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ConfigKey", SqlDbType.Int).Value = Config.ConfigKey;
                zCommand.Parameters.Add("@ConfigID", SqlDbType.NVarChar).Value = Config.ConfigID;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Config.Name;
                zCommand.Parameters.Add("@Value", SqlDbType.NVarChar).Value = Config.Value;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Config.Parent;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Config.Rank;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Config.Slug;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Config.PartnerNumber;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "201 Created";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SYS_Config SET "
                        + " ConfigID = @ConfigID,"
                        + " Name = @Name,"
                        + " Value = @Value,"
                        + " Parent = @Parent,"
                        + " Rank = @Rank,"
                        + " PartnerNumber = @PartnerNumber, Slug = @Slug"
                        + " WHERE ConfigKey = @ConfigKey";
            string zResult = "";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ConfigKey", SqlDbType.Int).Value = Config.ConfigKey;
                zCommand.Parameters.Add("@ConfigID", SqlDbType.NVarChar).Value = Config.ConfigID;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Config.Name;
                zCommand.Parameters.Add("@Value", SqlDbType.NVarChar).Value = Config.Value;
                zCommand.Parameters.Add("@Parent", SqlDbType.Int).Value = Config.Parent;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Config.Rank;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Config.Slug;
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = Config.PartnerNumber;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_Config SET RecordStatus = 99 WHERE ConfigKey = @ConfigKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ConfigKey", SqlDbType.Int).Value = Config.ConfigKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Empty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM SYS_Config WHERE ConfigKey = @ConfigKey";
            string zConnectionString = TN_Helper.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ConfigKey", SqlDbType.Int).Value = Config.ConfigKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                _Message = "200 OK";
            }
            catch (Exception Err)
            {
                _Message = "500" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
