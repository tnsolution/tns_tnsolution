﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Menu_WebSite_Data
    {
        public static List<Menu_WebSite_Model> List(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"WITH R AS 
(
    SELECT MenuKey, MenuID, MenuName, Action, Paramater, Publish, Rank, PARENT, DEPTH = 0, 
    SORT = CAST(MenuKey AS VARCHAR(MAX)), TypeKey, TypeName, PartnerNumber, RecordStatus, ContentTitle
    FROM PDT_Menu_WebSite
    WHERE RecordStatus <> 99 AND Parent = 0 AND PartnerNumber = @PartnerNumber
    UNION ALL
    SELECT Sub.MenuKey, Sub.MenuID, Sub.MenuName, Sub.Action, Sub.Paramater, Sub.Publish, Sub.Rank, Sub.PARENT, 
    DEPTH = R.DEPTH + 1, SORT = R.SORT + '-' + CAST(Sub.MenuKey AS VARCHAR(MAX)), Sub.TypeKey, Sub.TypeName, Sub.PartnerNumber, Sub.RecordStatus, Sub.ContentTitle
    FROM R INNER JOIN PDT_Menu_WebSite Sub ON R.MenuKey = Sub.Parent
    WHERE Sub.RecordStatus <> 99 AND Sub.PartnerNumber = @PartnerNumber
)
SELECT MenuName = REPLICATE('---', R.DEPTH * 1) + R.[MenuName], R.MenuKey, R.MenuID, R.Action, 
R.Paramater, R.Publish, R.Rank, R.SORT, R.DEPTH, R.Parent, R.TypeKey, R.TypeName, R.PartnerNumber, R.RecordStatus, R.ContentTitle
FROM R ORDER BY SORT, Rank";

            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Menu_WebSite_Model> zList = new List<Menu_WebSite_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Menu_WebSite_Model()
                {
                    MenuKey = r["MenuKey"].ToInt(),
                    MenuID = r["MenuID"].ToString(),
                    MenuName = r["MenuName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Parent = r["Parent"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Action = r["Action"].ToString(),
                    Paramater = r["Paramater"].ToString(),
                    TypeKey = r["TypeKey"].ToInt(),
                    TypeName = r["TypeName"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    ContentTitle = r["ContentTitle"].ToString(),
                });
            }
            return zList;
        }

        public static List<Menu_WebSite_Model> ListShow(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM PDT_Menu_WebSite WHERE RecordStatus <> 99 AND PartnerNumber = @PartnerNumber ORDER BY Rank";

            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Menu_WebSite_Model> zList = new List<Menu_WebSite_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Menu_WebSite_Model()
                {
                    MenuKey = r["MenuKey"].ToInt(),
                    MenuID = r["MenuID"].ToString(),
                    MenuName = r["MenuName"].ToString(),
                    Publish = r["Publish"].ToBool(),
                    Parent = r["Parent"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                    Action = r["Action"].ToString(),
                    Paramater = r["Paramater"].ToString(),
                    TypeKey = r["TypeKey"].ToInt(),
                    TypeName = r["TypeName"].ToString(),
                    PartnerNumber = r["PartnerNumber"].ToString(),
                    RecordStatus = r["RecordStatus"].ToInt(),
                    ContentTitle = r["ContentTitle"].ToString(),
                });
            }
            return zList;
        }
    }
}
