﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace WebSite
{
    public class Article_Category_Data
    {
        /// <summary>
        /// List chỉ hiển thị
        /// </summary>
        /// <param name="PartnerNumber"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static List<Article_Category_Model> ListAsMenu(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
WITH R AS 
( 
    SELECT CategoryKey, CategoryName AS NAME1, Description, Publish, 
    PARENT, DEPTH = 0, SORT = CAST(CategoryKey AS VARCHAR(MAX)) 
    FROM PDT_Article_Category 
    WHERE RecordStatus <> 99 
    AND Parent = 0 
    AND PartnerNumber = @PartnerNumber 
    AND Publish = 'TRUE'
    UNION ALL
    SELECT Sub.CategoryKey, Sub.CategoryName AS NAME1, Sub.Description, Sub.Publish, 
    Sub.PARENT, DEPTH = R.DEPTH + 1, SORT = R.SORT + '-' + CAST(Sub.CategoryKey AS VARCHAR(MAX)) 
    FROM R INNER JOIN PDT_Article_Category Sub ON R.CategoryKey = Sub.Parent 
    WHERE RecordStatus <> 99 
    AND PartnerNumber = @PartnerNumber
    AND Sub.Publish = 'TRUE'
)
SELECT CategoryName = R.[NAME1], R.CategoryKey, R.Description, R.Publish, R.SORT, R.DEPTH, R.Parent 
FROM R ORDER BY SORT  ";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Category_Model> zList = new List<Article_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    Description = r["Description"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Level = r["SORT"].ToInt(),
                });
            }
            return zList;
        }

        /// <summary>
        /// List xem xoá sửa
        /// </summary>
        /// <param name="PartnerNumber"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static List<Article_Category_Model> ListCategory(string PartnerNumber, out string Message)
        {
            DataTable zTable = new DataTable();
            string zSQL = "WITH R AS ( " +
                " SELECT CategoryKey, CategoryName, Description, Publish, PARENT, DEPTH = 0, SORT = CAST(CategoryKey AS VARCHAR(MAX)), Rank"
            + " FROM PDT_Article_Category "
            + " WHERE RecordStatus <> 99 "
            + " AND Parent = 0 "
            + " AND PartnerNumber = @PartnerNumber "
            + " UNION ALL "
            + " SELECT Sub.CategoryKey, Sub.CategoryName, Sub.Description, Sub.Publish, Sub.PARENT, DEPTH = R.DEPTH + 1, SORT = R.SORT + '-' + CAST(Sub.CategoryKey AS VARCHAR(MAX)), Sub.Rank"
            + " FROM R INNER JOIN PDT_Article_Category Sub ON R.CategoryKey = Sub.Parent "
            + " WHERE RecordStatus <> 99 "
            + " AND PartnerNumber = @PartnerNumber) "
            + " SELECT CategoryName = REPLICATE('---', R.DEPTH * 1) + R.CategoryName, R.CategoryKey, R.Description, R.Publish, R.SORT, R.DEPTH, R.Parent, R.Rank "
            + " FROM R ORDER BY SORT, Rank ";
            string zConnectionString = TN_Helper.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PartnerNumber", SqlDbType.NVarChar).Value = PartnerNumber;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close(); Message = string.Empty;
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            List<Article_Category_Model> zList = new List<Article_Category_Model>();
            foreach (DataRow r in zTable.Rows)
            {
                zList.Add(new Article_Category_Model()
                {
                    CategoryKey = r["CategoryKey"].ToInt(),
                    CategoryName = r["CategoryName"].ToString(),
                    Description = r["Description"].ToString(),
                    Parent = r["Parent"].ToInt(),
                    Publish = r["Publish"].ToBool(),
                    Level = r["SORT"].ToInt(),
                    Rank = r["Rank"].ToInt(),
                });
            }
            return zList;
        }
    }
}
