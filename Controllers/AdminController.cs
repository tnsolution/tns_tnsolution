﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
using HttpPostAttribute = Microsoft.AspNetCore.Mvc.HttpPostAttribute;
using JsonResult = Microsoft.AspNetCore.Mvc.JsonResult;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace WebSite.Controllers
{
    public class AdminController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        #region [--banner--]
        [Route("banner")]
        public IActionResult Banner()
        {
            ViewBag.ListBanner = Banner_Data.List(UserLog.PartnerNumber, out string Error);
            TempData["Error"] = Error;
            return View();
        }
        [HttpPost]
        [DisplayName("Cập nhật banner website")]
        public async Task<IActionResult> Save_Banner(IFormFile Banner, int BannerKey = 0, string Title = "", string Description = "", string Publish = "false", int Rank = 0)
        {
            Title ??= "";
            Description ??= "";

            var zInfo = new Banner_Info(BannerKey);
            zInfo.Banner.Rank = Rank;
            zInfo.Banner.Title = Title.Trim();
            zInfo.Banner.Description = Description.Trim();
            zInfo.Banner.Publish = Publish.ToBool();

            zInfo.Banner.Partnernumber = UserLog.PartnerNumber;
            zInfo.Banner.CreatedBy = UserLog.UserKey;
            zInfo.Banner.CreatedName = UserLog.UserName;
            zInfo.Banner.ModifiedBy = UserLog.UserKey;
            zInfo.Banner.ModifiedName = UserLog.UserName;

            string Uploaded = await TN_Helper.UploadAsync(Banner, "Banner");
            if (Uploaded.Length > 0)
            {
                zInfo.Banner.Url = Uploaded;
            }

            if (zInfo.Banner.BannerKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {

            }
            else
            {
                TempData["Error"] = zInfo.Message.GetFirstLine().GetFirstLine();
            }

            return RedirectToAction("Banner");
        }
        [HttpGet]
        [DisplayName("Chi tiết banner website")]
        public JsonResult Get_Banner(int BannerKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Banner_Info(BannerKey);
            var zModel = zInfo.Banner;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }
        [HttpPost]
        [DisplayName("Xóa banner website")]
        public JsonResult Delete_Banner(int BannerKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Banner_Info();
            zInfo.Banner.BannerKey = BannerKey;
            zInfo.Banner.ModifiedName = UserLog.UserName;
            zInfo.Banner.ModifiedBy = UserLog.UserKey;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }
            return Json(zResult);
        }
        public JsonResult Activate_Banner(int BannerKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Banner_Info();
            zInfo.Banner.ModifiedName = UserLog.UserName;
            zInfo.Banner.ModifiedBy = UserLog.UserKey;
            zInfo.SetActivate(BannerKey);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = string.Empty;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Data = string.Empty;
                zResult.Message = zInfo.Message.GetFirstLine();
            }
            return Json(zResult);
        }
        #endregion

        #region [--Danh mục--]
        [Route("danh-muc")]
        public IActionResult Article_Category()
        {
            ViewBag.ListCategory = Article_Category_Data.ListCategory(UserLog.PartnerNumber, out string Error);
            TempData["Error"] = Error;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Save_Article_Category(IFormFile ArticleBanner, int CategoryKey = 0, string CategoryName = "", string Description = "", int Parent = 0, int Rank = 0)
        {
            CategoryName ??= "";
            Description ??= "";

            var zInfo = new Article_Category_Info(CategoryKey);
            zInfo.Article_Category.CategoryName = CategoryName.Trim();
            zInfo.Article_Category.Description = Description.Trim();
            zInfo.Article_Category.Publish = true;
            zInfo.Article_Category.Parent = Parent;
            zInfo.Article_Category.Rank = Rank;
            zInfo.Article_Category.PartnerNumber = UserLog.PartnerNumber;
            zInfo.Article_Category.CreatedBy = UserLog.UserKey;
            zInfo.Article_Category.CreatedName = UserLog.UserName;
            zInfo.Article_Category.ModifiedBy = UserLog.UserKey;
            zInfo.Article_Category.ModifiedName = UserLog.UserName;

            string Uploaded = await TN_Helper.UploadAsync(ArticleBanner, "ArticleBanner");
            if (Uploaded.Length > 0)
            {
                zInfo.Article_Category.Image = Uploaded;
            }

            if (zInfo.Article_Category.CategoryKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {

            }
            else
            {
                TempData["Error"] = zInfo.Message.GetFirstLine().GetFirstLine();
            }

            return RedirectToAction("Article_Category");
        }
        [HttpGet]
        public JsonResult Edit_Article_Category(int CategoryKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Article_Category_Info(CategoryKey);
            var zModel = zInfo.Article_Category;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }
            return Json(zResult);
        }
        [HttpPost]
        public JsonResult Delete_Article_Category(int CategoryKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Article_Category_Info();
            zInfo.Article_Category.CategoryKey = CategoryKey;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }
            return Json(zResult);
        }
        [HttpPost]
        public JsonResult Publish_Article_Category(int CategoryKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Article_Category_Info();
            zInfo.Article_Category.ModifiedName = UserLog.UserName;
            zInfo.Article_Category.ModifiedBy = UserLog.UserKey;
            zInfo.SetPublish(CategoryKey);

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }
        #endregion

        #region[--Noi dung--]
        [Route("cac-bai-viet/{CategoryKey}")]
        public IActionResult Article_List(int CategoryKey)
        {
            ViewBag.CategoryName = new Article_Category_Info(CategoryKey).Article_Category.CategoryName;
            ViewBag.ListArticle = Article_Data.List(UserLog.PartnerNumber, CategoryKey, out string Error);
            TempData["Error"] = Error;
            return View("~/Views/Admin/Article_List.cshtml");

        }

        [Route("viet-bai/{CategoryKey}/{ArticleKey}")]
        public IActionResult Article_Edit(string ArticleKey, int CategoryKey)
        {
            ArticleKey ??= "";
            var zInfo = new Article_Info(ArticleKey);
            string CategoryName;
            if (ArticleKey == string.Empty)
            {
                CategoryName = new Article_Category_Info(CategoryKey).Article_Category.CategoryName;
            }
            else
            {
                CategoryName = zInfo.Article.CategoryName;
            }

            ViewBag.CategoryKey = CategoryKey;
            ViewBag.CategoryName = CategoryName;
            return View("~/Views/Admin/Article_Edit.cshtml", zInfo.Article);
        }

        /// <summary>
        /// Action
        /// </summary>
        /// <param name="ArticlePicture"></param>
        /// <param name="ArticleAttach"></param>
        /// <param name="ArticleKey"></param>
        /// <param name="ArticleName"></param>
        /// <param name="Summarize"></param>
        /// <param name="ArticleContent"></param>
        /// <param name="ArticleTag"></param>
        /// <param name="CategoryKey"></param>
        /// <param name="Publish"></param>
        /// <returns></returns>
        [HttpPost, ValidateInput(false)]
        public async Task<IActionResult> Save_Article(
            IFormFile ArticlePicture, IFormFile ArticleAttach,
            string ArticleKey = "", string ArticleName = "", string Summarize = "", string ArticleContent = "",
            string ArticleTag = "", int CategoryKey = 0)
        {
            string Error;
            try
            {
                ArticleKey ??= "";
                Summarize ??= "";
                ArticleContent ??= "";

                var zInfo = new Article_Info(ArticleKey);
                zInfo.Article.ArticleKey = ArticleKey;
                string ArticleID = ArticleName.Trim().ToAscii().ToLower();
                zInfo.Article.ArticleID = ArticleID;

                zInfo.Article.ArticleName = ArticleName.Trim();
                zInfo.Article.Summarize = Summarize.Trim();
                zInfo.Article.CategoryKey = CategoryKey.ToInt();
                zInfo.Article.ArticleTag = ArticleTag.Trim();
                zInfo.Article.ArticleContent = ArticleContent;
                zInfo.Article.Publish = true;
                zInfo.Article.PartnerNumber = UserLog.PartnerNumber;
                zInfo.Article.CreatedBy = UserLog.UserKey;
                zInfo.Article.CreatedName = UserLog.UserName;
                zInfo.Article.ModifiedBy = UserLog.UserKey;
                zInfo.Article.ModifiedName = UserLog.UserName;

                string Uploaded = await TN_Helper.UploadAsync(ArticlePicture, "Article");
                if (Uploaded.Length > 0)
                {
                    zInfo.Article.ImageLarge = Uploaded;
                }

                Uploaded = await TN_Helper.UploadAsync(ArticleAttach, "Article");
                if (Uploaded.Length > 0)
                {
                    zInfo.Article.FileAttack = Uploaded;
                }

                if (zInfo.Article.ArticleKey == "")
                {
                    zInfo.Create_ServerKey();
                }
                else
                {
                    zInfo.Update();
                }

                if (zInfo.Code == "200" ||
                    zInfo.Code == "201")
                {
                    return RedirectToAction("Article_List", new { CategoryKey });
                }
                else
                {
                    Error = zInfo.Message.GetFirstLine().GetFirstLine();
                }
            }
            catch (Exception ex)
            {
                Error = ex.ToString().GetFirstLine();
            }
            TempData["Error"] = Error;

            return View("~/Views/Admin/Article_Edit.cshtml");
        }

        /// <summary>
        /// Action
        /// </summary>
        /// <param name="CategoryKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete_Article(string ArticleKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Article_Info();
            zInfo.Article.ArticleKey = ArticleKey;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }

        /// <summary>
        /// Action
        /// </summary>
        /// <param name="ArticleKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Activate_Article(string ArticleKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Article_Info();
            zInfo.Article.ModifiedName = UserLog.UserName;
            zInfo.Article.ModifiedBy = UserLog.UserKey;
            zInfo.SetPublish(ArticleKey);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }
            return Json(zResult);
        }
        [HttpPost]
        public JsonResult Check_ArticleID(string ArticleKey, string ArticleName)
        {
            var zResult = new ServerResult();

            int Count = 0;
            string ActicleID = ArticleName.Trim().ToAscii().ToLower();
            Count = Article_Data.CheckActicleID(ArticleKey, ArticleName, UserLog.PartnerNumber, out string Message);
            if (Count == 0)
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
                zResult.Data = ActicleID;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = Message;
            }

            return Json(zResult);
        }
        #endregion

        #region [--Menu website--]
        [Route("Menu-website")]
        public IActionResult Menu()
        {
            string Error;
            ViewBag.ListCategory = Article_Category_Data.ListCategory(UserLog.PartnerNumber, out Error);
            TempData["Error"] += Error;
            ViewBag.ListArticle = Article_Data.ListForSelect(UserLog.PartnerNumber, out Error);
            TempData["Error"] += Error;
            ViewBag.ListMenu = Menu_WebSite_Data.List(UserLog.PartnerNumber, out Error);
            TempData["Error"] += Error;
            return View();
        }
        [HttpPost]
        [DisplayName("Cập nhật menu website")]
        public IActionResult Save_Menu(
            int MenuKey = 0, string MenuName = "", string ParamaterArticle = "",
            string ParamaterCategory = "", string ArticleName = "", string CategoryName = "",
            int Type = 0, string Publish = "false", int Parent = 0, int Rank = 0)
        {
            MenuName ??= "";
            ParamaterArticle ??= "";
            ParamaterCategory ??= "";

            var zInfo = new Menu_WebSite_Info(MenuKey);
            zInfo.Menu_WebSite.Rank = Rank;
            zInfo.Menu_WebSite.MenuName = MenuName;
            zInfo.Menu_WebSite.TypeKey = Type;

            if (Type == 1)
            {
                Article_Info zArt = new Article_Info(ParamaterArticle);

                zInfo.Menu_WebSite.TypeName = "Bài viết chi tiết";
                zInfo.Menu_WebSite.Action = "Article";
                zInfo.Menu_WebSite.Paramater = zArt.Article.ArticleID;
                zInfo.Menu_WebSite.ContentTitle = ArticleName;
            }
            if (Type == 2)
            {
                zInfo.Menu_WebSite.TypeName = "Danh mục tin tức";
                zInfo.Menu_WebSite.Action = "Article_List";
                zInfo.Menu_WebSite.Paramater = ParamaterCategory;
                zInfo.Menu_WebSite.ContentTitle = CategoryName;
            }

            zInfo.Menu_WebSite.Parent = Parent;
            zInfo.Menu_WebSite.Publish = Publish.ToBool();
            zInfo.Menu_WebSite.PartnerNumber = UserLog.PartnerNumber;

            if (zInfo.Menu_WebSite.MenuKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }

            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {

            }
            else
            {
                TempData["Error"] = zInfo.Message.GetFirstLine().GetFirstLine();
            }

            return RedirectToAction("Menu");
        }
        [HttpGet]
        [DisplayName("Chi tiết Menu website")]
        public JsonResult Get_Menu(int MenuKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Menu_WebSite_Info(MenuKey);
            var zModel = zInfo.Menu_WebSite;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }
            return Json(zResult);
        }
        [HttpPost]
        [DisplayName("Xóa Menu website")]
        public JsonResult Delete_Menu(int MenuKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Menu_WebSite_Info();
            zInfo.Menu_WebSite.MenuKey = MenuKey;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }
            return Json(zResult);
        }
        public JsonResult Activate_Menu(int MenuKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Menu_WebSite_Info();
            zInfo.SetActivate(MenuKey);
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }
            return Json(zResult);
        }
        #endregion

        #region [--Config--]
        [Route("tuy-chinh")]
        public IActionResult Config()
        {
            string Logo = "";
            var zInfo = new Partner_Info(UserLog.PartnerNumber);
            if (zInfo.Partner.Logo_Large.Length > 0)
            {
                Logo = zInfo.Partner.Logo_Large;
            }

            ViewBag.Logo = Logo;
            ViewBag.ListCategory = Article_Category_Data.ListCategory(UserLog.PartnerNumber, out string Error);
            TempData["Error"] += Error;
            ViewBag.ListConfig = Config_Data.List(UserLog.PartnerNumber, out Error);
            TempData["Error"] = Error;
            ViewBag.ListArticle = Article_Data.ListForSelect(UserLog.PartnerNumber, out Error);
            TempData["Error"] += Error;

            return View("~/Views/Admin/Config.cshtml");
        }

        public async Task<JsonResult> Change_Logo(IFormFile Logo)
        {
            var zResult = new ServerResult();
            try
            {
                var zInfo = new Partner_Info(UserLog.PartnerNumber);
                string Uploaded = await TN_Helper.UploadAsync(Logo, "Logo");

                if (Uploaded != string.Empty)
                {
                    zInfo.Partner.ModifiedBy = UserLog.UserKey;
                    zInfo.Partner.ModifiedName = UserLog.UserName;
                    zInfo.Partner.Logo_Large = Uploaded;
                    zInfo.ChangeLogo();
                    zResult.Success = true;
                }
                else
                {
                    zResult.Success = false;
                    zResult.Message = "Bạn chưa chọn Logo !.";
                }
            }
            catch (Exception ex)
            {
                zResult.Success = false;
                zResult.Message = ex.ToString().GetFirstLine();
            }

            return Json(zResult);
        }

        /// <summary>
        /// Action
        /// </summary>
        /// <param name="ConfigKey"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult Get_Config(int ConfigKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Config_Info(ConfigKey);
            var zModel = zInfo.Config;
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Data = JsonConvert.SerializeObject(zModel);
                return Json(zResult);
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
                return Json(zResult);
            }
        }

        /// <summary>
        /// Action
        /// </summary>
        /// <param name="ConfigKey"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete_Config(int ConfigKey)
        {
            var zResult = new ServerResult();
            var zInfo = new Config_Info();
            zInfo.Config.ConfigKey = ConfigKey;
            zInfo.Delete();
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine();
            }

            return Json(zResult);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ConfigKey"></param>
        /// <param name="Name"></param>
        /// <param name="Value"></param>
        /// <param name="Parent"></param>
        /// <param name="Rank"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Save_Config(int ConfigKey, string ConfigID, string Name, string Value, int Parent, int Rank, int Slug)
        {
            var zResult = new ServerResult();
            var zInfo = new Config_Info(ConfigKey);

            zInfo.Config.Slug = Slug;
            zInfo.Config.ConfigKey = ConfigKey;
            zInfo.Config.ConfigID = ConfigID.Trim();
            zInfo.Config.Name = Name.Trim();
            zInfo.Config.Value = Value.Trim();
            zInfo.Config.Parent = Parent.ToInt();
            zInfo.Config.Rank = Rank.ToInt();
            zInfo.Config.PartnerNumber = UserLog.PartnerNumber;

            if (zInfo.Config.ConfigKey == 0)
            {
                zInfo.Create_ServerKey();
            }
            else
            {
                zInfo.Update();
            }
            if (zInfo.Code == "200" ||
                zInfo.Code == "201")
            {
                zResult.Success = true;
                zResult.Message = string.Empty;
            }
            else
            {
                zResult.Success = false;
                zResult.Message = zInfo.Message.GetFirstLine().GetFirstLine();
            }

            return Json(zResult);
        }
        #endregion

        //Admin/UploadImage
        [HttpPost]
        public async Task<JsonResult> UploadImage(IFormFile file)
        {
            string saveimg = "";
            string message = "";
            try
            {
                string Uploaded = await TN_Helper.UploadAsync(file, "Article");
                if (Uploaded != string.Empty)
                {
                    saveimg = Uploaded;
                    message = "The selected file" + file.FileName + " is save";
                }
            }
            catch (Exception ex)
            {
                message = ex.ToString().GetFirstLine();
            }

            return Json(new { location = Url.Content(saveimg) });
        }
    }
}