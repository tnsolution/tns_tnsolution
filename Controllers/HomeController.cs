﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using WebSite.Models;

namespace WebSite.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _Iconfiguration;

        public HomeController(ILogger<HomeController> logger, IConfiguration iconfiguration)
        {
            _logger = logger;
            _Iconfiguration = iconfiguration;

            TN_Helper.ConnectionString = _Iconfiguration.GetSection("Data").GetSection("ConnectionString").Value;
            TN_Helper.PartnerNumber = _Iconfiguration.GetSection("PartnerNumber").Value;
        }

        public IActionResult Index()
        {


            return View();
        }

        [Route("bai-viet/{ArticleID}")]
        public IActionResult Article(string ArticleID)
        {
            var zInfo = new Article_Info();
            zInfo.Get_ArticleID(ArticleID);
            if (zInfo.Code == "404" ||
                zInfo.Article.Publish == false ||
                zInfo.Article.RecordStatus == 99)
            {
                return View("~/Views/Shared/404.cshtml");
            }

            ViewBag.CategoryName = new Article_Category_Info(zInfo.Article.CategoryKey).Article_Category.CategoryName;

            return View("~/Views/Home/Article.cshtml", zInfo.Article);
        }

        [Route("tin-tuc/{CategoryKey}")]
        public IActionResult Article_List(int CategoryKey)
        {
            ViewBag.CategoryName = new Article_Category_Info(CategoryKey).Article_Category.CategoryName;
            var zList = Article_Data.ListShow(TN_Helper.PartnerNumber, CategoryKey, out _);
            return View("~/Views/Home/Article_List.cshtml", zList);
        }

        [Route("lien-he")]
        public IActionResult Contact()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View("~/Views/Shared/404.cshtml", new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            //return View();
        }

        [HttpPost]
        public JsonResult SendMail(string Email, string Name, string Title, string Content)
        {
            var zResult = new ServerResult();
            if (Email == null && Email == string.Empty)
            {
                zResult.Success = false;
                zResult.Message = "Mail không đúng định dạng !.";
                return Json(zResult);
            }

            Name ??= "";
            Title ??= "";
            Content ??= "";

            var zModel = new EmailModel
            {
                Email = _Iconfiguration.GetSection("MailInfo").GetSection("MailFrom").Value,
                Password = _Iconfiguration.GetSection("MailInfo").GetSection("Password").Value,
                To = _Iconfiguration.GetSection("MailInfo").GetSection("MailTo").Value,
            };

            if (Name != string.Empty &&
                Title != string.Empty &&
                Content != string.Empty)
            {
                string Body =
                   "Họ tên:" + Name + "<br/>" +
                   "Email: " + Email + "<br/>" +
                   "Nội dung:" + Content + "<br/>";

                zModel.Subject = Title;
                zModel.Body = Body;
            }
            else
            {
                Title = "Nhận đăng ký thông tin";
                string Body = "Nhận thông tin gửi đến mail:" + Email;
                zModel.Subject = Title;
                zModel.Body = Body;
            }

            zResult.Message = TN_Utils.SendGmail(zModel);
            return Json(zResult);
        }
    }
}
